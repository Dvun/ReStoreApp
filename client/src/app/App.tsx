import React, {Suspense} from 'react'
import './App.scss'
import Layout from '../components/layout'
import Navbar from '../components/navbar'
import AppRoutes from '../components/routes'

const App = () => {

  return (
    <>
      <Navbar/>
      <Layout>
        <Suspense fallback={<div>Loading...</div>}>
          <AppRoutes/>
        </Suspense>
      </Layout>
    </>
  )
}

export default App
